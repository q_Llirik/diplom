﻿using Desktop.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Pages
{
    /// <summary>
    /// Логика взаимодействия для ReferencesListPage.xaml
    /// </summary>
    public partial class ReferencesListPage : Page, IUpdaterPage
    {
        /// <summary>
        /// Список всех заявок 
        /// </summary>
        private static IEnumerable<Reference> referencesList;
        /// <summary>
        /// Список изменённых заявок
        /// </summary>
        private static List<ReferenceStatusChange> changedList;

        public ReferencesListPage()
        {
            InitializeComponent();
        }

        public void LoadTabe(IEnumerable<Reference> refs)
        {
            spTable.Children.Clear();
            if (refs == null || refs.Count() == 0)
            {
                var txb = new TextBlock {
                    Text = "Результатов не найдено",
                    FontSize = 18,
                    Foreground = Brushes.LightGray,
                    FontStyle = FontStyles.Italic,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(10)
                };
                spTable.Children.Add(txb);
                return;
            }
            foreach(var i in refs.OrderByDescending(w=>w.Date))
            {
                var refer = new ReferenceRow(i, ReferenceRowMode.ChangeStatus);
                spTable.Children.Add(refer);
            }
        }

        private void Search_TC(object sender, TextChangedEventArgs e)
        {
            if (tbxFullNameSearch.Text.Length == 0)
            {
                LoadTabe(referencesList);
                return;
            }
            LoadTabe(referencesList.Where(w=>w.FullName.Contains(tbxFullNameSearch.Text)));
        }

        private void History_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.navigation.OpenPage(new HistoryPage());
        }

        private void PageLoad_L(object sender, RoutedEventArgs e)
        {
            UpdatePage();
        }

        public void UpdatePage()
        {
            referencesList = ApiWorking.GetNotInHistoryReferences();
            changedList = new List<ReferenceStatusChange>();

            LoadTabe(referencesList);
        }
    }
}
