﻿using Desktop.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Pages
{
    /// <summary>
    /// Логика взаимодействия для HistoryPage.xaml
    /// </summary>
    public partial class HistoryPage : Page, IUpdaterPage
    {
        private List<Reference> historyList = new List<Reference>();

        public HistoryPage()
        {
            InitializeComponent();
        }

        public void LoadTabe(IEnumerable<Reference> refs)
        {
            spTable.Children.Clear();
            if (refs.Count() == 0)
            {
                var txb = new TextBlock
                {
                    Text = "История пуста",
                    FontSize = 18,
                    Foreground = Brushes.DarkGray,
                    FontStyle = FontStyles.Italic,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(10)
                };
                spTable.Children.Add(txb);
                return;
            }
            foreach (var i in refs)
            {
                spTable.Children.Add(new ReferenceRow(i, ReferenceRowMode.NoChangeStatus));
            }
        }

        public void UpdatePage()
        {
            historyList = ApiWorking.GetHistoryReferences()?.ToList();
            LoadTabe(historyList);
        }

        private void HistoryClear_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить следующие заявки?", App.AppName, MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                var result = ApiWorking.DeleteReferences(historyList.Select(w=>w.UserID));
                if (!result.IsWell)
                {
                    MessageBox.Show("При удалении что-то пошло не так: " + '\n' + result.Error.Message, App.AppName + "-Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                PageLoaded_L(null,null);
            }
        }

        private void ListReferences_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.navigation.OpenPage(new ReferencesListPage());
        }

        private void PageLoaded_L(object sender, RoutedEventArgs e)
        {
            UpdatePage();
        }
    }
}
