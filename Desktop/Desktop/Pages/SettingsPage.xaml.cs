﻿using Desktop.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Pages
{
    /// <summary>
    /// Логика взаимодействия для SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            InitializeComponent();
        }

        private void PageLoad_L(object sender, RoutedEventArgs e)
        {
            var set = App.Setting;
            chbxSounds.IsChecked = set.Sound;
            tbxApiHost.Text = set.HostName;
            tbxApiPort.Text = set.Port + "";
            tbxCBHost.Text = set.CallBackHostName;
            tbxCBPort.Text = set.CallBackPort + "";
        }

        private void SettingsSave_Click(object sender, RoutedEventArgs e)
        {
            Settings set = null;
            try
            {
                set = new Settings
                {
                    Sound = chbxSounds.IsChecked.Value,
                    HostName = tbxApiHost.Text,
                    Port = int.Parse(tbxApiPort.Text),
                    CallBackHostName = tbxCBHost.Text,
                    CallBackPort = int.Parse(tbxCBPort.Text)
                };
            }
            catch 
            {
                MessageBox.Show("Введите корректные данные.", App.AppName + "-Error", MessageBoxButton.OK ,MessageBoxImage.Error);
                return;
            }
            App.Setting = set;
            MessageBox.Show("Настройки сохранены.", App.AppName, MessageBoxButton.OK,MessageBoxImage.Information);
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.navigation.BackPage();
        }
    }
}
