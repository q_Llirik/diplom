﻿using Desktop.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Pages
{
    /// <summary>
    /// Логика взаимодействия для ReferenceRow.xaml
    /// </summary>
    public partial class ReferenceRow : UserControl
    {
        private SolidColorBrush InWorkStatusColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#adadbd"));
        private SolidColorBrush TaketStatusColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#0202ff"));
        private SolidColorBrush ReadyStatusColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#29bc36"));
        private SolidColorBrush HistoryStatusColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#65657a"));

        private Reference reference;
        public ReferenceRow(Reference selectRef, ReferenceRowMode mode)
        {
            reference = selectRef;
            InitializeComponent();
            imResource = null;
            txbFullName.Text = reference.FullName;
            txbDateTime.Text = reference.Date.ToShortDateString();
            txbGroupNumber.Text = reference.GroupNumber;
            txbCount.Text = reference.Count + "";
            btnNextStatusChanged.Visibility = mode == ReferenceRowMode.ChangeStatus ? Visibility.Visible : Visibility.Collapsed;
            if (mode == ReferenceRowMode.ChangeStatus)
                UpDateBTN(selectRef.Status);
            SelectBackground(selectRef.Status);
        }

        private void UpDateBTN(ReferenceStatuses status)
        {
            btnNextStatusChanged.Content = GetButtonTitle(status);
            btnNextStatusChanged.Tag = status;
        }

        private string GetButtonTitle(ReferenceStatuses status)
        {
            switch(status)
            {
                case ReferenceStatuses.Новая: { return "Принять в обработку"; }
                case ReferenceStatuses.Принят: { return "Справка готова"; }
                case ReferenceStatuses.Готово: { return "В историю"; }
                default: { return ""; }
            }
        }

        private void SelectBackground(ReferenceStatuses status)
        {
            switch (status)
            {
                case ReferenceStatuses.Новая: { row.Background = InWorkStatusColor; break; }
                case ReferenceStatuses.Принят: { row.Background = TaketStatusColor; break; }
                case ReferenceStatuses.Готово: { row.Background = ReadyStatusColor; break; }
                case ReferenceStatuses.История: { row.Background = HistoryStatusColor; break; }
            }
        }

        private void NexttStatusChanged_Click(object sender, RoutedEventArgs e)
        {
            var status = (ReferenceStatuses)(((int)btnNextStatusChanged.Tag) + 1); // Получает статус на который был изменён.
            var mes = string.Format(@"Вы уверены, что хотите изменить статус у заявки - {0} на ""{1}""?", reference.ToString(), status);
            if (MessageBox.Show(mes, App.AppName, MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.No)
            {
                return;
            }
            var result = ApiWorking.ChangeReferenceStatus(new List<ReferenceStatusChange> { new ReferenceStatusChange(reference.UserID, status) });
            if (!result.IsWell)
            {
                MessageBox.Show(String.Format("Произошла ошибка: {0} - {1}", result.Error.Code, result.Error.Message), App.AppName + " - Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MainWindow.navigation.LastUpdate();
            if (status != ReferenceStatuses.История)
                if (!ApiWorking.SendNotificationToUser(reference, status))
                {
                    MessageBox.Show("Уведомление пользователю о смене статуса заявки не отправлено по неизвестным причинам.", App.AppName + " - Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            MessageBox.Show("Статус успешно изменён.", App.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }

    public enum ReferenceRowMode
    {
        ChangeStatus,
        NoChangeStatus
    }
}
