﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VkNet;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace Desktop.Tools
{
    public class ApiWorking
    {
        private static string APINAME => "ReferencesAPI";

        public static bool CheckAPIWorking()
        {
            try
            { 
                var uri = string.Format("http://{0}:{1}/", App.Setting.HostName, App.Setting.Port);
                var result = JsonConvert.DeserializeObject<ReferencesAPIResponse<string>>(GetAnswer(WebRequest.Create(uri)));
                return result == null ? false : true;
            }
            catch (Exception ex)
            {
                return false;
            }
}

        /// <summary>
        /// Получает заявки, не входящие в историю
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Reference> GetNotInHistoryReferences()
        {
            try
            {
                var uri = string.Format("http://{0}:{1}/{2}/{3}", App.Setting.HostName, App.Setting.Port, APINAME, "GetNotInHistoryReferences");
                var result = JsonConvert.DeserializeObject<ReferencesAPIResponse<IEnumerable<Reference>>>(GetAnswer(WebRequest.Create(uri)));
                return result == null || result.IsWell == false ? new List<Reference>() : result.Body;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, App.AppName + " - Error", MessageBoxButton.OK ,MessageBoxImage.Error);
                return null;
            }
        }

        /// <summary>
        /// Получает заявки из истории
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Reference> GetHistoryReferences()
        {
            try
            { 
                var uri = string.Format("http://{0}:{1}/{2}/{3}", App.Setting.HostName, App.Setting.Port, APINAME, "GetHistoryReferences");
                var result = JsonConvert.DeserializeObject<ReferencesAPIResponse<IEnumerable<Reference>>>(GetAnswer(WebRequest.Create(uri)));
                return result == null || result.IsWell == false ? new List<Reference>() : result.Body;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, App.AppName + " - Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static ReferencesAPIResponse<string> ChangeReferenceStatus(IEnumerable<IReferenceStatusChange> changes)
        {
            var uri = string.Format("http://{0}:{1}/{2}/{3}", App.Setting.HostName, App.Setting.Port, APINAME, "ChangeRefStatus");
            var str = Task.Run(() => PostAnswer(uri, "PUT", JsonConvert.SerializeObject(changes)));
            var result = JsonConvert.DeserializeObject<ReferencesAPIResponse<string>>(str.Result);
            return result;
        }

        public static ReferencesAPIResponse<string> DeleteReferences(IEnumerable<long> deleteList)
        {
            var uri = string.Format("http://{0}:{1}/{2}/{3}", App.Setting.HostName, App.Setting.Port, APINAME, "DeleteRefs");
            var str = Task.Run(() => PostAnswer(uri, "DELETE", JsonConvert.SerializeObject(deleteList)));
            var result = JsonConvert.DeserializeObject<ReferencesAPIResponse<string>>(str.Result);
            return result;
        }

        /// <summary>
        /// Отправялет Get запрос
        /// </summary>
        /// <param name="uri">URI запроса</param>
        /// <returns></returns>
        private static string GetAnswer(WebRequest request)
        {
            var answer = "";
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var line = "";
                        while ((line = reader.ReadLine()) != null)
                        {
                            answer += line;
                        }
                    }
                }
                response.Close();
            }
            catch (Exception ex)
            {
                return "";
            }
            return answer;
        }

        /// <summary>
        /// Отправляет Put запрос
        /// </summary>
        /// <param name="uri">URI для запроса</param>
        /// <param name="json">Тело запроса</param>
        /// <returns></returns>
        private static string PostAnswer(string uri, string Method, string json)
        {
            WebRequest request = WebRequest.Create(uri);
            request.Method = Method; 
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;
            
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            var str = GetAnswer(request);
            return str;
        }

        /// <summary>
        /// Отправка пользователю уведомления об изменении статуса заявки
        /// </summary>
        /// <param name="reference">Заявка</param>
        /// <param name="statuses">Статус</param>
        /// <returns>Результат</returns>
        public static bool SendNotificationToUser(Reference reference, ReferenceStatuses statuses)
        {
            
            switch(reference.Type)
            {
                case ReferenceType.VK: { return SendRefStatToUserVK(reference.UserID, statuses); } 
                case ReferenceType.Telegram: { return false; } //в следующем обновлении
                default: return false;
            }
        }

        /// <summary>
        /// Отправка уведомления пользователю ВКонтакте
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="status">Статус</param>
        /// <returns>Результат</returns>
        private static bool SendRefStatToUserVK(long userId, ReferenceStatuses status)
        {
            var vkApi = new VkApi();
            vkApi.Authorize(new ApiAuthParams() { AccessToken = Settings.VkToken});

            var rnd = new Random();
            var message = new MessagesSendParams {
                UserId = userId,
                RandomId = rnd.Next(),
                Message = GetMessageByStatus(status)
            };
            try
            {
                vkApi.Messages.Send(message);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Отправляемые уведомления на основе статуса
        /// </summary>
        /// <param name="status">Статус</param>
        /// <returns>Уведомление</returns>
        private static string GetMessageByStatus(ReferenceStatuses status)
        {
            switch(status)
            {
                case ReferenceStatuses.Принят: { return "Ваша заявка принята в обработку."; }
                case ReferenceStatuses.Готово: { return "Справки по вашей заявке готовы. Можете их забрать."; }
                default: { return String.Empty; }
            }
        }
    }

    public class ReferencesAPIResponse<T>
    {
        [JsonProperty("IsWell")]
        public bool IsWell { get; set; }
        [JsonProperty("Body")]
        public T Body { get; set; }
        [JsonProperty("Error")]
        public ReferenceAPIError Error { get; set; }
    }

    public class ReferenceAPIError
    {
        [JsonProperty("Code")]
        public int Code { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }
    }
}
