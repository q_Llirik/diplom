﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Tools
{
    /// <summary>
    /// Класс для работы с CallBackAPI
    /// </summary>
    public class CallBackHelper
    {
        private UdpClient udpClient;
        private IPEndPoint ipPoint;
        public CallBackHelper()
        {
            ipPoint = new IPEndPoint(IPAddress.Parse(App.Setting.CallBackHostName), App.Setting.CallBackPort);
            udpClient = new UdpClient(ipPoint);
        }

        public void StartListening()
        {
            udpClient.BeginReceive(Receive, new object());
        }

        private void Receive(IAsyncResult ar)
        {
            byte[] bytes = udpClient.EndReceive(ar, ref ipPoint);
            string message = Encoding.UTF8.GetString(bytes);
            var reference = JsonConvert.DeserializeObject<Reference>(message);
            NewReferenceEvent(reference);
            StartListening();
        }

        public delegate void NewReferenceDelegate(Reference newReference);
        public event NewReferenceDelegate NewReferenceEvent;
    }
}
