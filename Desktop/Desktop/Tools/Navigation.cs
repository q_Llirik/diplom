﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Desktop.Tools
{
    public class Navigation
    {
        private MainWindow main;
        private List<Page> pagesList;

        public Navigation(MainWindow mainWindow)
        {
            main = mainWindow;
            pagesList = new List<Page>();
        }

        public void LastUpdate()
        {
            ((IUpdaterPage)pagesList.Last()).UpdatePage();
        }

        public void OpenPage(Page page)
        {
            if (pagesList.Count != 0 && pagesList.Last().GetType() == page.GetType())
            {
                return;
            }
            pagesList.Add(page);
            main.txbTitle.Text = String.Format("{0} - {1}", App.AppName, page.Title);
            main.frmCont.Navigate(page);            
        }

        public void BackPage()
        {
            pagesList.Remove(pagesList.Last());
            main.txbTitle.Text = String.Format("{0} - {1}", App.AppName, pagesList.Last().Title);
            main.frmCont.Navigate(pagesList.Last());
        }
    }
}
