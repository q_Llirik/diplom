﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Tools
{
    public class Reference : IReference
    {
        [JsonProperty("UserID")]
        public long UserID { get; set; }
        [JsonProperty("Type")]
        public ReferenceType Type { get; set; }
        [JsonProperty("FullName")]
        public string FullName { get; set; }
        [JsonProperty("GroupNumber")]
        public string GroupNumber { get; set; }
        [JsonProperty("Count")]
        public int Count { get; set; }
        [JsonProperty("Date")]
        public DateTime Date { get; set; }
        [JsonProperty("Status")]
        public ReferenceStatuses Status { get; set; }

        public Reference(long userID, ReferenceType type, string fullName, string groupNumber, int count, DateTime date, ReferenceStatuses status)
        {
            UserID = userID;
            Type = type;
            FullName = fullName;
            GroupNumber = groupNumber;
            Count = count;
            Date = date;
            Status = status;
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}, {3}", FullName, Date, GroupNumber, Count);
        }
    }
}
