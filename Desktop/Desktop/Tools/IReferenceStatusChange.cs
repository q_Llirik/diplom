﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Tools
{
    public interface IReferenceStatusChange
    {
        long UserID { get; set; }
        ReferenceStatuses Status { get; set; }
    }
}
