﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Desktop.Tools
{
    [Serializable]
    public class Settings
    {
        public static string VkToken => @"291717baa8d671684d738444f70807a616e91081fab2511ae23d8f6ab62e3bf6b45d30cbba2232be435e6";
        /// <summary>
        /// Звук
        /// </summary>
        [XmlElement("SOUND")]
        public bool Sound { get; set; }
        /// <summary>
        /// Хост API
        /// </summary>
        [XmlElement("HOSTNAME")]
        public string HostName { get; set; }
        /// <summary>
        /// Порт API
        /// </summary>
        [XmlElement("PORT")]
        public int Port { get; set; }
        /// <summary>
        /// Хост CallBack
        /// </summary>
        private string callBackHostName;
        [XmlElement("CALLBACK_HOSTNAME")]
        public string CallBackHostName
        {
            get
            {
                if (callBackHostName == "localhost")
                    callBackHostName = "127.0.0.1";
                return callBackHostName;
            }
            set
            {
                callBackHostName = value;
            }
        }
        /// <summary>
        /// Порт CallBack
        /// </summary>
        [XmlElement("CALLBACK_PORT")]
        public int CallBackPort { get; set; }
    }

    public class SettingsHelper
    {
        private static string FileName => "Settings.cfg";
        /// <summary>
        /// Загрузка настроек
        /// </summary>
        /// <returns></returns>
        public static Settings Load()
        {
            var formatter = new XmlSerializer(typeof(Settings));
            if (!File.Exists(FileName))
            {
                var settings = new Settings() {
                    HostName = "localhost",
                    Port = 49974,
                    CallBackHostName = "localhost",
                    CallBackPort = DateTime.Now.Year
                };
                SaveSettings(settings);
            }
            using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                return (Settings)formatter.Deserialize(fs); 
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        /// <param name="settings">Настройки</param>
        /// <returns>Удачность сохранения</returns>
        public static bool SaveSettings(Settings settings)
        {
            if (File.Exists(FileName))
                File.Delete(FileName);
            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
                {
                    var formatter = new XmlSerializer(typeof(Settings));
                    formatter.Serialize(fs, settings);
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }
    }
}
