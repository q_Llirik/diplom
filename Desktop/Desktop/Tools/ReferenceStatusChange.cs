﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Tools
{
    public class ReferenceStatusChange : IReferenceStatusChange
    {
        public long UserID { get; set; }
        public ReferenceStatuses Status { get; set; }

        public ReferenceStatusChange(long userID, ReferenceStatuses status)
        {
            UserID = userID;
            Status = status;
        }

        public override string ToString()
        {
            return String.Format("Статус заявки (ID пользователя - {0}) изменён на {1}", UserID, Status);
        }
    }
}
