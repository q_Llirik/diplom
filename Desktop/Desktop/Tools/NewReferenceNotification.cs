﻿using Notifications.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Desktop.Tools
{
    public class NewReferenceNotification
    {
        private SoundPlayer player;
        private NotificationManager notificationManager;
        private Reference NewReference;

        public NewReferenceNotification(Reference newReference)
        {
            player = new SoundPlayer(Properties.Resources.Windows_Balloon);
            notificationManager = new NotificationManager();
            NewReference = newReference;
        }

        public void ShowNotification()
        {
            try
            {
                notificationManager.Show(new NotificationContent() {
                    Title = "Новая заявка",
                    Message = String.Format("Получена новая заявка на получение справки от {0}.", NewReference.FullName),
                    Type = NotificationType.Information
                });
                player.Play();
            }
            catch 
            {
            }
        }
    }
}
