﻿using Desktop.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Desktop
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string AppName => "СПРАВКИ";
        private static Settings settings;
        public static Settings Setting {
            get
            {
                if (settings == null)
                    settings = SettingsHelper.Load();
                return settings;
            }
            set
            {
                SettingsHelper.SaveSettings(value);
                settings = value;
            }
        }
    }
}
