﻿using Desktop.Pages;
using Desktop.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Navigation navigation;
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool CheckApiWorking()
        {
            if (!ApiWorking.CheckAPIWorking())
            {
                MessageBox.Show("Система API недоступна. Возможно у вас отсутствует интернет соединение. Последующая работа приложения без API системы невозможна.", App.AppName + "-Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        private void CallBackHelper_NewReferenceEvent(Reference newReference)
        {
            new NewReferenceNotification(newReference).ShowNotification();
            Update_MLBD(null,null);
        }

        public static void OpenPage(Page page) => navigation.OpenPage(page);

        public static void BackPage() => navigation.BackPage();

        private void DragMove_MLBD(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void CloseWindow_MLBD(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Minimalized_MLBD(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Update_MLBD(object sender, MouseButtonEventArgs e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, 
                (ThreadStart)delegate() {
                    var page = (IUpdaterPage)frmCont.Content;
                    page.UpdatePage();
                });
        }

        private void MainWindowLoaded_L(object sender, RoutedEventArgs e)
        {
            if (!CheckApiWorking())
            {
                //Environment.Exit(0);
            }

            navigation = new Navigation(this);
            navigation.OpenPage(new ReferencesListPage());

            CallBackHelper callBackHelper = new CallBackHelper();
            callBackHelper.StartListening();
            callBackHelper.NewReferenceEvent += CallBackHelper_NewReferenceEvent;
        }

        private void SettingsOpen_MLBD(object sender, MouseButtonEventArgs e)
        {
            if (frmCont.Content == new SettingsPage())
            {
                return;
            }
            MainWindow.navigation.OpenPage(new SettingsPage());
        }

        private void FrmCont_Navigating(object sender, NavigatingCancelEventArgs e)
        {
        }
    }
}
