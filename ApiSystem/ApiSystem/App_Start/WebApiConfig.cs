﻿using ApiSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ApiSystem
{
    public static class WebApiConfig
    {
        private static Settings settings;
        public static Settings Settings
        {
            get
            {
                if (settings == null)
                    settings = SettingsHelper.Load();
                return settings;
            }
        }
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{*catchall}",
                defaults: new { controller = "ReferencesAPI", action = "Default", catchall = RouteParameter.Optional }
            );
        }
    }
}
