﻿namespace ApiSystem.Models
{
    public interface IReferenceStatusChange
    {
        long UserID { get; set; }
        ReferenceStatuses Status { get; set; }
    }
}
