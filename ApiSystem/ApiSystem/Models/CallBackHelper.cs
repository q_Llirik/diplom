﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ApiSystem.Models
{
    public class CallBackHelper
    {
        public static void SendNewReference(IReference reference)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(WebApiConfig.Settings.CallbackHostName), WebApiConfig.Settings.CallbackPort);
            var datagram = JsonConvert.SerializeObject(reference);
            byte[] bytes = Encoding.UTF8.GetBytes(datagram);
            Task.Run(() => SendNewMessage(ipPoint, bytes));
        }

        /// <summary>
        /// Отправляет новое сообщение методом связи UDP
        /// </summary>
        /// <param name="ip">Адрес</param>
        /// <param name="bytes">Сообщение в виде битов</param>
        private static void SendNewMessage(IPEndPoint ip, byte[] bytes)
        {
            UdpClient sender = new UdpClient();
            try
            {
                sender.Send(bytes, bytes.Length, ip);
            }
            catch { }
            finally
            {
                sender.Close();
            }
        }
    }
}