﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ApiSystem.Models
{
    public class DBWorker
    {
        private static string DBName => "RefsDB";
        private static string TableName => "References";
        private static string ConnectionString => WebApiConfig.Settings.ConnString;
        private static SqlConnection sqlConnection;

        /// <summary>
        /// Делает запрос к БД и получает ответ в виде SqlDataReader.
        /// </summary>
        /// <param name="Query">Запрос в виде строки</param>
        /// <returns></returns>
        private static SqlDataReader RequestToDataBase(string Query)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);
                sqlConnection.Open();
                SqlCommand command = new SqlCommand(Query, sqlConnection);
                var reader = command.ExecuteReader();
                return reader;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Выполняет запрос к базе данных, который не нуждается в чтении ответа.
        /// </summary>
        /// <param name="query">Пользовательский запрос.</param>
        private static bool NonExecuteQuery(string query)
        {
            try
            {
                sqlConnection = new SqlConnection(ConnectionString);
                sqlConnection.Open();
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.ExecuteNonQuery();
                sqlConnection.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Получение заявок по условия.
        /// </summary>
        /// <param name="Condition">Условие в виде sql запроса.</param>
        /// <returns></returns>
        private static IEnumerable<Reference> GetRefWithCondition(string Condition)
        {
            string query = String.Format("Select * From [{0}] {1}", TableName, Condition);
            var reader = RequestToDataBase(query);
            if (reader == null)
                return null;
            var returnList = new List<Reference>();
            while(reader.Read())
            {
                returnList.Add(new Reference(reader.GetInt32(1), (ReferenceType)reader.GetInt32(2), reader.GetString(3), 
                    reader.GetString(4), reader.GetInt32(5), reader.GetDateTime(6), (ReferenceStatuses) reader.GetInt32(7)));
            }
            sqlConnection.Close();
            return returnList;
        }

        /// <summary>
        /// Получает заявки, находящиеся в обработке 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Reference> GetNotInHistoryReferences()
        {
            return GetRefWithCondition(String.Format("where {0} <> {1} AND {2} <> {3}", "[StatusID]", '4', "[IsDeleted]", "1"));
        }

        /// <summary>
        /// Получает заявки, находящиеся в истории 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Reference> GetHistoryReferences()
        {
            return GetRefWithCondition(String.Format("where {0} = {1} AND {2} <> {3}", "[StatusID]", '4', "[IsDeleted]", "1"));
        }

        /// <summary>
        /// Получает заявку по ID пользователя
        /// </summary>
        /// <param name="UserID">ID пользователя</param>
        /// <returns></returns>
        public static Reference GetRefByUserID(long UserID) => GetRefWithCondition(String.Format("where [UserID] = {0} AND {1} <> {2} AND {3} <> {4}", UserID, "[IsDeleted]", "1", "StatusID", "4")).LastOrDefault();

        /// <summary>
        /// Добавляет новую заявку в БД.
        /// </summary>
        /// <param name="reference">Заявка</param>
        /// <returns>Возвращает ошибку, если такова есть. Проверять на null.</returns>
        public static bool AddNewRefToDB(IReference reference)
        {
            var query = String.Format("INSERT INTO [{0}]([UserID], [Type], [FullName], [GroupNumber], [Count], [Datetime], [StatusID], [IsDeleted]) values ({1}, {2}, N'{3}', N'{4}', {5}, Cast(N'{6}' as datetime), {7}, {8})",
            TableName, reference.UserID, (int)reference.Type, reference.FullName, reference.GroupNumber, reference.Count, reference.Date, 1, 0);
            return NonExecuteQuery(query);      
        }

        /// <summary>
        /// Изменяет статус заявки
        /// </summary>
        /// <param name="UserID">ID пользователя</param>
        /// <param name="Status">Статус</param>
        /// <returns></returns>
        public static bool ChangeRefStatus(long UserID, ReferenceStatuses Status)
        {
            var query = String.Format("UPDATE [{0}] SET [StatusID] = {1} WHERE [UserID] = {2} AND {3} <> {4} AND {5} <> {6}", TableName, (int)Status, UserID, "[StatusID]", "4", "[IsDeleted]", "1");
            return NonExecuteQuery(query);
        }

        /// <summary>
        /// Удаляет запись по ID пользователя
        /// </summary>
        /// <param name="UserID">ID пользователя</param>
        /// <returns></returns>
        public static bool DeleteRef(long UserID)
        {
            var query = String.Format("UPDATE [{0}] SET [IsDeleted] = 1 WHERE [UserID] = {1} AND {2} = {3}", TableName, UserID, "[StatusID]", "4");
            return NonExecuteQuery(query);
        }
    }
}