﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiSystem.Models
{
    public interface IReference
    {
        long UserID { get; set; }
        ReferenceType Type { get; set; }
        string FullName { get; set; }
        string GroupNumber { get; set; }
        int Count { get; set; }
        DateTime Date { get; set; }
        ReferenceStatuses Status { get; set; }
    }

    public enum ReferenceType
    {
        VK = 1,
        Telegram = 2
    }

    public enum ReferenceStatuses
    {
        ВОбработке = 1,
        Принят = 2,
        Готово = 3,
        ВИстории = 4
    }
}
