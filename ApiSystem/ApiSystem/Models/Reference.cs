﻿using System;

namespace ApiSystem.Models
{
    public class Reference : IReference
    {
        public long UserID { get; set; }
        public ReferenceType Type { get;set; }
        public string FullName { get; set; }
        public string GroupNumber { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
        public ReferenceStatuses Status { get; set; }

        public Reference(long userID, ReferenceType type, string fullName, string groupNumber, int count, DateTime date, ReferenceStatuses status = ReferenceStatuses.ВОбработке)
        {
            UserID = userID;
            Type = type;
            FullName = fullName;
            GroupNumber = groupNumber;
            Count = count;
            Date = date;
            Status = status;
        }        
    }
}