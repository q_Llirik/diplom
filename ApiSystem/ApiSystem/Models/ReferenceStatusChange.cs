﻿namespace ApiSystem.Models
{
    public class ReferenceStatusChange : IReferenceStatusChange
    {
        public long UserID { get; set; }
        public ReferenceStatuses Status { get; set; }
    }
}