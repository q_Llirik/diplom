﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ApiSystem.Models
{
    [Serializable]
    public class Settings
    {
        private string callBackHostName;
        [XmlElement("CALLBACK_HOSTNAME")]
        public string CallbackHostName {
            get
            {
                if (callBackHostName == "localhost")
                    callBackHostName = "127.0.0.1";
                return callBackHostName;
            }
            set
            {
                callBackHostName = value;
            }
        }
        [XmlElement("CALLBACK_PORT")]
        public int CallbackPort { get; set; }
        [XmlElement("CONNECTIONSTRING")]
        public string ConnString { get; set; }
    }

    public class SettingsHelper
    {
        private static string FileName => "Settings.cfg";
        /// <summary>
        /// Загрузка настроек
        /// </summary>
        /// <returns></returns>
        public static Settings Load()
        {
            var formatter = new XmlSerializer(typeof(Settings));
            if (!File.Exists(FileName))
            {
                var settings = new Settings()
                {
                    CallbackHostName = "127.0.0.1",
                    CallbackPort = DateTime.Now.Year,
                    ConnString = "Data Source=.\\SQLEXPRESS;Initial Catalog=RefsDB;Integrated Security=true;"
                };
                SaveSettings(settings);
            }
            using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                return (Settings)formatter.Deserialize(fs);
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        /// <param name="settings">Настройки</param>
        /// <returns>Удачность сохранения</returns>
        public static bool SaveSettings(Settings settings)
        {
            if (File.Exists(FileName))
                File.Delete(FileName);
            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
                {
                    var formatter = new XmlSerializer(typeof(Settings));
                    formatter.Serialize(fs, settings);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}