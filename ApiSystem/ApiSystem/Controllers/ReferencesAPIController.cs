﻿using ApiSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Results;

namespace ApiSystem.Controllers
{
    public class ReferencesAPIController : ApiController
    {
        [HttpGet]
        public JsonResult<ReferencesAPIResponse<string>> Default() => Json(new ReferencesAPIResponse<string>(true, "Я готов к работе."));
        
        /// <summary>
        /// Получить заявки, находящиеся в обработке
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult<ReferencesAPIResponse<IEnumerable<Reference>>> GetNotInHistoryReferences()
        {
            var result = DBWorker.GetNotInHistoryReferences();
            if (result == null)
            {
                return Json(new ReferencesAPIResponse<IEnumerable<Reference>>(false, null,
                    new ReferenceAPIError(0, "Неизвестная ошибка.")));
            }
            if (result.Count() == 0)
            {
                return Json(new ReferencesAPIResponse<IEnumerable<Reference>>(false, null, 
                    new ReferenceAPIError(0, "В таблице нет таких записей.")));
            }
            return Json(new ReferencesAPIResponse<IEnumerable<Reference>>(true, result));
        }

        /// <summary>
        /// Получить заявки, находящиеся в истории
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult<ReferencesAPIResponse<IEnumerable<Reference>>> GetHistoryReferences()
        {
            var result = DBWorker.GetHistoryReferences();
            if (result == null)
            {
                return Json(new ReferencesAPIResponse<IEnumerable<Reference>>(false, null,
                    new ReferenceAPIError(0, "Неизвестная ошибка.")));
            }
            if (result.Count() == 0)
            {
                return Json(new ReferencesAPIResponse<IEnumerable<Reference>>(false, null,
                    new ReferenceAPIError(0, "В таблице нет таких записей.")));
            }
            return Json(new ReferencesAPIResponse<IEnumerable<Reference>>(true, result));
        }

        /// <summary>
        /// Получить заявку по ID пользователя
        /// </summary>
        /// <param name="catchall"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult<ReferencesAPIResponse<Reference>> GetRefByUserID(string catchall)
        {
            if (!Regex.IsMatch(catchall, @"userid=\d"))
            {
                return Json(new ReferencesAPIResponse<Reference>(false, null,
                    new ReferenceAPIError(400, "Переданные параметры не соответствуют формату: userid=000000000")));
            }

            var spl = catchall.Split(new [] {'='}, StringSplitOptions.RemoveEmptyEntries);
            var resultRef = DBWorker.GetRefByUserID(long.Parse(spl.Last()));
            if (resultRef == null)
            {
                return Json(new ReferencesAPIResponse<Reference>(false, null,
                    new ReferenceAPIError(0, "Неизвестная ошибка.")));
            }
            if (resultRef == null)
            {
                return Json(new ReferencesAPIResponse<Reference>(false, null,
                    new ReferenceAPIError(404, "Заявки с таким ID пользователя не существует")));
            }
            return Json(new ReferencesAPIResponse<Reference>(true, resultRef));
        }

        /// <summary>
        /// Добавить новую заявку
        /// </summary>
        /// <param name="reference">Новая заявка</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult<ReferencesAPIResponse<string>> AddNewRef([FromBody] Reference reference)
        {
            var resultRef = DBWorker.GetRefByUserID(reference.UserID);
            if (resultRef != null)
            {
                return Json(new ReferencesAPIResponse<string>(false, null,
                    new ReferenceAPIError(401, "Зявка у данного пользователя уже существует в БД.")));
            }

            var result = DBWorker.AddNewRefToDB(reference);
            if (!result)
            {
                return Json(new ReferencesAPIResponse<string>(false, null,
                    new ReferenceAPIError(400, "Неизвестная ошибка")));
            }
            else
            {
                CallBackHelper.SendNewReference(reference);
            }

            return Json(new ReferencesAPIResponse<string>(true, "OK"));
        }

        /// <summary>
        /// Изменение статуса заявки
        /// </summary>
        /// <param name="referenceStatusChange"></param>
        /// <returns></returns>
        [HttpPut]
        public JsonResult<ReferencesAPIResponse<string>> ChangeRefStatus([FromBody] List<ReferenceStatusChange> referenceStatusChangeList)
        {
            if (referenceStatusChangeList == null || referenceStatusChangeList?.Count() == 0)
            {
                return Json(new ReferencesAPIResponse<string>(false, null, 
                    new ReferenceAPIError(400, "Список пуст либо json-запрос неверный.")));
            }
            foreach(var i in referenceStatusChangeList)
            {
                var checkRefHave = DBWorker.GetRefByUserID(i.UserID);
                if (checkRefHave == null)
                {
                    return Json(new ReferencesAPIResponse<string>(false, null,
                        new ReferenceAPIError(400, "Зявки с ID пользователя " + i.UserID + " не существует.")));
                }

                var result = DBWorker.ChangeRefStatus(i.UserID, i.Status);
                if (!result)
                {
                    return Json(new ReferencesAPIResponse<string>(false, null,
                        new ReferenceAPIError(400, "Неизвестная ошибка")));
                }
            }

            return Json(new ReferencesAPIResponse<string>(true, "OK"));
        }

        /// <summary>
        /// Удаление заявок
        /// </summary>
        /// <param name="deleteList">Коллекция ID пользователей</param>
        /// <returns></returns>
        [HttpDelete]
        public JsonResult<ReferencesAPIResponse<string>> DeleteRefs([FromBody] List<long> deleteList)
        {
            if (deleteList == null || deleteList?.Count == 0)
            {
                return Json(new ReferencesAPIResponse<string>(false, null,
                    new ReferenceAPIError(400, "Список пуст либо json-запрос неверный.")));
            }

            foreach(var i in deleteList)
            {
                var result = DBWorker.DeleteRef(i);
                if (!result)
                {
                    return Json(new ReferencesAPIResponse<string>(false, null,
                        new ReferenceAPIError(400, "Неизвестная ошибка")));
                }
            }

            return Json(new ReferencesAPIResponse<string>(true, "OK"));
        }

        /// <summary>
        /// Документация по API-системе
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult<string> GetApiInfo()
        {
            var mes = "На данный момент в API-системе пристутствуют следующие методы: " + 
                "GetNotInHistoryReferences - GET - Получить заявки, находящиеся в обработке;" + 
                "GetHistoryReferences - GET - Получить заявки, находящиеся в истории;" + 
                "GetRefByUserID - GET - Получить заявку по ID пользователя;" + 
                "AddNewRef - POST - Добавить новую заявку;" + 
                "ChangeRefStatus - PUT - Изменение статуса заявки;" + 
                "DeleteRefs - DELETE - Удаление заявок.";
            return Json(mes);
        }
    }
}