﻿namespace ApiSystem.Controllers
{
    public class ReferencesAPIResponse<T>
    {
        public bool IsWell { get; set; }
        public T Body { get; set; }
        public ReferenceAPIError Error { get; set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="isWell">Показатель удачности</param>
        /// <param name="body">Тело ответа</param>
        /// <param name="error">Ошибка, если есть</param>
        public ReferencesAPIResponse(bool isWell, T body, ReferenceAPIError error = null) 
        {
            IsWell = isWell;
            Body = body;
            Error = error;
        }
    }

    public class ReferenceAPIError
    {
        public int Code { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="code">Код ошибки</param>
        /// <param name="message">Тело ошибки</param>
        public ReferenceAPIError(int code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}