﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkBot.Core
{
    class RequestForm
    {
        public long UserID { get; set; }
        public int Type => 1;
        public string FullName { get; set; }
        public string GroupNumber { get; set; }
        public int Count { get; set; }
        private DateTime dt => DateTime.Now;
        public DateTime Date => new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);


        public Mode GetRequestMode()
        {
            if (Count != 0)
                return Mode.End;

            if (GroupNumber != null)
                return Mode.Count;

            if (FullName != null)
                return Mode.GroupNumber;

            if (UserID != 0)
                return Mode.FullName;

            return Mode.End;
        }

        public enum Mode
        {
            FullName,
            GroupNumber,
            Count,
            End 
        }

        public RequestForm(long UserID)
        {
            this.UserID = UserID;
        }

        public JObject ToJson() => JObject.FromObject(this);
    }
}
