﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkBot.Keyboards;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace VkBot.Core
{
    class MessagesHandling
    {
        static public List<MessagesSendParams> GetSendMessage(Message ReceiveMessage)
        {
            return BotKeyboards.BotKeyboardsWorking[
                BotKeyboardTitles.KeyboardsTitleList.Contains(ReceiveMessage.Body)? 
                ReceiveMessage.Body:BotKeyboardTitles.Default]
                .DynamicInvoke(new object[] { ReceiveMessage}) as List<MessagesSendParams>;
        }
    }
}
