﻿using VkBot.Core;

namespace VkBot.Messages
{
    class SendMessageTitles
    {
        public static string Default => "Слушаю Вас.";
        public static string InputFullName => "Введите Ваше ФИО. Пример: Иванов Иван Иванович.";
        public static string IncorrectlyFullName => "Введённое ФИО некорректно. Повторите ввод. Пример: Иванов Иван Иванович.";
        public static string InputGroupNumber => "Введите номер Вашей группы.";
        public static string IncorrectlyGroupNumber => "Введённый номер группы некорректен. Повторите ввод.";
        public static string InputCount => "Введите количество справок.";
        public static string IncorrectlyCount => "Введённое количество справок некорректно. Повторите ввод.";
        public static string RequestAgree => "Заявка принята. При изменении статуса завки Вам будут приходить уведомления.";
        public static string ErrorRequest => "Произошла неизвестная ошибка. Пожалуйста, повторите отправку заявки позднее.";
        public static string NoMoreErrorRequest => "Пока ваша прошлая заявка находится в обработке, новые добавлять нельзя.";

        public static string Agree(RequestForm request)
        {
            return "Подтвердите вашу заявку. После подтверждения изменить заявку не удастся." + '\n'
                                + "ФИО: " + request.FullName + '\n'
                                + "Группа: " + request.GroupNumber + '\n'
                                + "Количество: " + request.Count + '\n';
        }
    }
}
