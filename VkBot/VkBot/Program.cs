﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkBot.Core;
using VkBot.Tools;
using VkNet;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace VkBot
{
    class Program
    {
        static public VkApi vkApi = new VkApi();

        static void Main(string[] args)
        {
            var vkApi = new VkApi();
            vkApi.Authorize(new ApiAuthParams() { AccessToken = AppConfig.TOKEN });

            while (true)
            {
                var s = vkApi.Groups.GetLongPollServer(AppConfig.GROUPID);
                var poll = vkApi.Groups.GetBotsLongPollHistory(
                                      new BotsLongPollHistoryParams()
                                      {
                                          Server = s.Server,
                                          Ts = s.Ts,
                                          Key = s.Key,
                                          Wait = 10
                                      });

                

                if (poll?.Updates == null) continue;

                foreach (var update in poll.Updates.Where(w=>w.Type == GroupUpdateType.MessageNew))
                {
                    LoggingMessages.LogReceivedMessage(update.Message);

                    foreach (var message in MessagesHandling.GetSendMessage(update.Message))
                    {
                        vkApi.Messages.Send(message);
                        LoggingMessages.LogSendMessage(message);
                    }
                }
            }
        }
    }
}
