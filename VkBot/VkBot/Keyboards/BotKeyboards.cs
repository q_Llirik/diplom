﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;
using static VkBot.Keyboards.BotKeyboardsProcedures;

namespace VkBot.Keyboards
{
    class BotKeyboards
    {
        public static List<MessageKeyboardButton> BotKeyboardsList => typeof(BotKeyboards).GetProperties().Where(w => w.GetType() == typeof(MessageKeyboardButton)).Select(w=>w.GetValue(w) as MessageKeyboardButton).ToList();

        public static MessageKeyboardButton NewRequestKeyboard => new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Primary,
            Action = new MessageKeyboardButtonAction() { Label = BotKeyboardTitles.NewRequest }
        };

        public static MessageKeyboardButton AgreeKeyboard => new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Positive,
            Action = new MessageKeyboardButtonAction() { Label = BotKeyboardTitles.Agree }
        };

        public static MessageKeyboardButton CancelKeyboard => new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Negative,
            Action = new MessageKeyboardButtonAction() { Label = BotKeyboardTitles.Cancel }
        };

        private static MessageKeyboardButton DefaultKeyboard => new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Negative,
            Action = new MessageKeyboardButtonAction() { Label = BotKeyboardTitles.Cancel }
        };

        public static MessageKeyboardButton OKKeyboard => new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Positive,
            Action = new MessageKeyboardButtonAction() { Label = BotKeyboardTitles.OK }
        };

        public static Dictionary<string, Delegate> BotKeyboardsWorking = new Dictionary<string, Delegate> {
            { BotKeyboardTitles.NewRequest, new ReceiveMessageDelegate(NewRequestProcedure)},
            { BotKeyboardTitles.Agree, new ReceiveMessageDelegate(AgreeProcedure)},
            { BotKeyboardTitles.Cancel, new ReceiveMessageDelegate(CancelProcedure)},
            {BotKeyboardTitles.OK, new ReceiveMessageDelegate(OKProcedure)},
            { BotKeyboardTitles.Default, new ReceiveMessageDelegate(DefaultProcedure)},
        };
    }
}
