﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VkBot.Core;
using VkBot.Messages;
using VkBot.Tools;
using VkNet.Model;
using VkNet.Model.RequestParams;
using static VkBot.Core.RequestForm;

namespace VkBot.Keyboards
{
    class BotKeyboardsProcedures
    {
        private static List<RequestForm> UsersRequests = new List<RequestForm>();

        public delegate List<MessagesSendParams> ReceiveMessageDelegate(Message ReceiveMessage);

        public static List<MessagesSendParams> NewRequestProcedure(Message ReceiveMessage)
        {
            UsersRequests.Add(new RequestForm(ReceiveMessage.UserId.Value));
            return new List<MessagesSendParams> {
                MessageConstructor.CreateMessage(SendMessageTitles.InputFullName, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.CancelKeyboard})
            };
        }

        public static List<MessagesSendParams> AgreeProcedure(Message ReceiveMessage)
        {
            if ((UsersRequests.FirstOrDefault(w => w.UserID == ReceiveMessage.UserId)?.GetRequestMode() == Mode.End))
            {
                var result = APIWorking.AddReferenceToDB(UsersRequests.FirstOrDefault(w => w.UserID == ReceiveMessage.UserId));
                if (result.IsWell == false)
                {
                    if (result.Error.Code == 401)
                    {
                        UsersRequests.Remove(UsersRequests.FirstOrDefault(w => w.UserID == ReceiveMessage.UserId));
                        return new List<MessagesSendParams>()
                        {
                            MessageConstructor.CreateMessage(SendMessageTitles.NoMoreErrorRequest, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.OKKeyboard})
                        };
                    }
                    else
                    {
                        UsersRequests.Remove(UsersRequests.FirstOrDefault(w=>w.UserID == ReceiveMessage.UserId));
                        return new List<MessagesSendParams>()
                        {
                            MessageConstructor.CreateMessage(SendMessageTitles.ErrorRequest, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.OKKeyboard})
                        };
                    }
                }
                else
                {
                    UsersRequests.Remove(UsersRequests.FirstOrDefault(w => w.UserID == ReceiveMessage.UserId));
                    return new List<MessagesSendParams>()
                    {
                        MessageConstructor.CreateMessage(SendMessageTitles.RequestAgree, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.OKKeyboard})
                    };
                }
            }
            else
            {
                return DefaultProcedure(ReceiveMessage);
            }
        }

        public static List<MessagesSendParams> CancelProcedure(Message ReceiveMessage)
        {
            UsersRequests.Remove(UsersRequests.FirstOrDefault(w=>w.UserID == ReceiveMessage.UserId.Value));
            return new List<MessagesSendParams> {
                MessageConstructor.CreateMessage(SendMessageTitles.Default, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.NewRequestKeyboard})
            };
        }

        public static List<MessagesSendParams> OKProcedure(Message ReceiveMessage)
        {
            UsersRequests.Remove(UsersRequests.FirstOrDefault(w => w.UserID == ReceiveMessage.UserId.Value));
            return new List<MessagesSendParams> {
                MessageConstructor.CreateMessage(SendMessageTitles.Default, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.NewRequestKeyboard})
            };
        }

        public static List<MessagesSendParams> DefaultProcedure(Message ReceiveMessage)
        {
            var request = UsersRequests.FirstOrDefault(w => w.UserID == ReceiveMessage.UserId);

            if (request != null)
            {
                switch(request.GetRequestMode())
                {
                    case Mode.FullName:
                        {
                            if (!CheckFullName(ReceiveMessage.Body))
                                return new List<MessagesSendParams> {
                                    MessageConstructor.CreateMessage(SendMessageTitles.IncorrectlyFullName, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.CancelKeyboard})
                                };

                            request.FullName = ReceiveMessage.Body;
                            return new List<MessagesSendParams> {
                                MessageConstructor.CreateMessage(SendMessageTitles.InputGroupNumber, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.CancelKeyboard})
                            };
                        }
                    case Mode.GroupNumber:
                        {
                            if (String.IsNullOrEmpty(ReceiveMessage.Body))
                            {
                                return new List<MessagesSendParams> {
                                    MessageConstructor.CreateMessage(SendMessageTitles.IncorrectlyGroupNumber, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.CancelKeyboard})
                                };
                            }
                            request.GroupNumber = ReceiveMessage.Body;
                            return new List<MessagesSendParams> {
                                MessageConstructor.CreateMessage(SendMessageTitles.InputCount, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.CancelKeyboard})
                            };
                        }
                    case Mode.Count:
                        {
                            if (!CheckGroupNumberAndCount(ReceiveMessage.Body))
                                return new List<MessagesSendParams> {
                                    MessageConstructor.CreateMessage(SendMessageTitles.IncorrectlyCount, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.CancelKeyboard})
                                };

                            request.Count = int.Parse(ReceiveMessage.Body);
                            return new List<MessagesSendParams> {
                                MessageConstructor.CreateMessage(SendMessageTitles.Agree(request), ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.AgreeKeyboard, BotKeyboards.CancelKeyboard})
                            };
                        }
                    case Mode.End:
                        {
                            return new List<MessagesSendParams> {
                                MessageConstructor.CreateMessage(SendMessageTitles.Agree(request), ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.AgreeKeyboard, BotKeyboards.CancelKeyboard})
                            };
                        }
                    default:
                        {
                            return new List<MessagesSendParams> {
                                MessageConstructor.CreateMessage(SendMessageTitles.Default, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.NewRequestKeyboard})
                            };
                        }
                }
            }
            else
            {
                return new List<MessagesSendParams> {
                    MessageConstructor.CreateMessage(SendMessageTitles.Default, ReceiveMessage.UserId.Value, new List<VkNet.Model.Keyboard.MessageKeyboardButton> { BotKeyboards.NewRequestKeyboard})
                };
            }
        }

        /// <summary>
        /// Проверка ФИО на корректность
        /// </summary>
        /// <param name="FullName">ФИО</param>
        /// <returns></returns>
        private static bool CheckFullName(string FullName)
        {
            if (String.IsNullOrEmpty(FullName))
                return false;
            var regex = @"^\p{Lu}\p{Ll}*(?:-\p{Lu}\p{Ll}*)? \p{Lu}\p{Ll}*(?:-\p{Lu}\p{Ll}*)? \p{Lu}\p{Ll}*(?:-\p{Lu}\p{Ll}*)?$";
            return Regex.IsMatch(FullName, regex);
        }

        /// <summary>
        /// Проверка номера группы и количества справок на корректность
        /// </summary>
        /// <param name="check">Номер группы или количество справок</param>
        /// <returns></returns>
        private static bool CheckGroupNumberAndCount(string check)
        {
            var i = 0;
            if (!int.TryParse(check, out i))
                return false;
            return true;
        }
    }
}
