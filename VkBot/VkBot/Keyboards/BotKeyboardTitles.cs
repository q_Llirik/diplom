﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkBot.Keyboards
{
    class BotKeyboardTitles
    {
        public static List<string> KeyboardsTitleList => new List<string>() { NewRequest, Agree, Cancel};

        public static string NewRequest => "Новая заявка";
        public static string Agree => "Подтвердить";
        public static string Cancel => "Отменить";
        public static string OK => "OK";
        public static string Default => "";
    }
}
