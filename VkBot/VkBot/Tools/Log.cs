﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkBot.Tools
{
    class Log
    {
        /// <summary>
        /// Вывод лога в консоль. Вид: Debug.
        /// </summary>
        /// <param name="Message">Лог</param>
        public static void d(string Message)
        {
            Console.Write(DateTime.Now + "|");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("DEBUG");
            Console.ResetColor();
            Console.WriteLine("|" + Message);
        }

        /// <summary>
        /// Вывод лога в консоль. Вид: Information.
        /// </summary>
        /// <param name="Message">Лог</param>
        public static void i(string Message)
        {
            Console.Write(DateTime.Now + "|");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("INFO");
            Console.ResetColor();
            Console.WriteLine("|" + Message);
        }

        /// <summary>
        /// Вывод лога в консоль. Вид: Error.
        /// </summary>
        /// <param name="Message">Лог</param>
        public static void e(string Message)
        {
            Console.Write(DateTime.Now + "|");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("ERROR");
            Console.ResetColor();
            Console.WriteLine("|" + Message);
        }

        /// <summary>
        /// Вывод лога в консоль. Вид: Trace.
        /// </summary>
        /// <param name="Message">Лог</param>
        public static void t(string Message)
        {
            Console.Write(DateTime.Now + "|");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("TRACE");
            Console.ResetColor();
            Console.WriteLine("|" + Message);
        }
    }
}
