﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace VkBot.Tools
{
    [Serializable]
    public class Settings
    {
        [XmlElement("HOSTNAME")]
        public string HostName { get; set; }
        [XmlElement("PORT")]
        public int Port { get; set; }
    }

    public class SettingsHelper
    {
        private static string FileName => "Settings.cfg";
        /// <summary>
        /// Загрузка настроек
        /// </summary>
        /// <returns></returns>
        public static Settings Load()
        {
            var formatter = new XmlSerializer(typeof(Settings));
            if (!File.Exists(FileName))
            {
                var settings = new Settings()
                {
                    HostName = "localhost",
                    Port = 49974
                };
                SaveSettings(settings);
            }
            using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                return (Settings)formatter.Deserialize(fs);
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        /// <param name="settings">Настройки</param>
        /// <returns>Удачность сохранения</returns>
        public static bool SaveSettings(Settings settings)
        {
            if (File.Exists(FileName))
                File.Delete(FileName);
            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
                {
                    var formatter = new XmlSerializer(typeof(Settings));
                    formatter.Serialize(fs, settings);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
