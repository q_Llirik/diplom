﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace VkBot.Tools
{
    class LoggingMessages
    {
        /// <summary>
        /// Вывод информации по принятым сообщениям.
        /// </summary>
        /// <param name="ReceivedMessage">Принятое сообщение.</param>
        static public void LogReceivedMessage(Message ReceivedMessage)
        {
            Program.vkApi.Authorize(new ApiAuthParams() { AccessToken = AppConfig.TOKEN });
            var user = Program.vkApi.Users.Get(new List<long> { ReceivedMessage.UserId.Value }).First();
            Log.t(user.FirstName + " " + user.LastName + "(" + user.Id + ")" + ":" + ReceivedMessage.Body);
        }

        /// <summary>
        /// Вывод информации по отправленным сообщениям.
        /// </summary>
        /// <param name="ReceivedMessage">Отправленное сообщение.</param>
        static public void LogSendMessage(MessagesSendParams SentMessage)
        {
            Program.vkApi.Authorize(new ApiAuthParams() { AccessToken = AppConfig.TOKEN });
            var user = Program.vkApi.Users.Get(new List<long> { SentMessage.UserId.Value }).First();
            Log.t("БОТ" + ":" + SentMessage.Message.Replace('\n', '/'));
        }
    }
}
