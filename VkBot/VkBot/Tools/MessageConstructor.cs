﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace VkBot.Tools
{
    class MessageConstructor
    {
        /// <summary>
        /// Создание отправляемого сообщения из данных
        /// </summary>
        /// <param name="MessageBody">Текст сообщения</param>
        /// <param name="UserID">Уникальный идентификатор пользователя</param>
        /// <param name="ButtonsList">Коллекция кнопок</param>
        /// <returns></returns>
        public static MessagesSendParams CreateMessage(string MessageBody, long UserID, List<MessageKeyboardButton> ButtonsList)
        {
            Random rnd = new Random();
            var resultKeyboard = new MessagesSendParams()
            {
                Message = MessageBody,
                RandomId = rnd.Next(),
                UserId = UserID,
                Keyboard = new MessageKeyboard
                {
                    OneTime = true,
                    Buttons = new List<List<MessageKeyboardButton>> { ButtonsList }
                }
            };

            return resultKeyboard;
        }
        /// <summary>
        /// Создание отправляемого сообщения из данных
        /// </summary>
        /// <param name="MessageBody">Текст сообщения</param>
        /// <param name="UserID">Уникальный идентификатор пользователя</param>
        /// <returns></returns>
        public static MessagesSendParams CreateMessage(string MessageBody, long UserID)
        {
            Random rnd = new Random();
            return new MessagesSendParams()
            {
                Message = MessageBody,
                RandomId = rnd.Next(),
                UserId = UserID
            };
        }
    }
}
