﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using VkBot.Core;

namespace VkBot.Tools
{
    class APIWorking
    {
        private static string APIName => "ReferencesAPI";

        public static ReferencesAPIResponse<string> AddReferenceToDB(RequestForm requestForm)
        {
            try
            {
                var uri = string.Format("http://{0}:{1}/{2}/{3}", AppConfig.Settings.HostName, AppConfig.Settings.Port, APIName, "AddNewRef");
                var str = Task.Run(() => PostAnswer(uri, "POST", JsonConvert.SerializeObject(requestForm)));
                return JsonConvert.DeserializeObject<ReferencesAPIResponse<string>>(str.Result);
            }
            catch (Exception ex)
            {
                return new ReferencesAPIResponse<string>() { IsWell = false, Body = "", Error = new ReferenceAPIError() { Code = 400, Message = ex.Message} };
            }
        }
        /// <summary>
        /// Отправялет Get запрос
        /// </summary>
        /// <param name="uri">URI запроса</param>
        /// <returns></returns>
        private static async Task<string> GetAnswer(WebRequest request)
        {
            var answer = "";
            try
            {
                WebResponse response = await request.GetResponseAsync().ConfigureAwait(false);
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        var line = "";
                        while ((line = reader.ReadLine()) != null)
                        {
                            answer += line;
                        }
                    }
                }
                response.Close();
            }
            catch (Exception ex)
            {
                Log.e(ex.Message);
            }
            return answer;
        }

        /// <summary>
        /// Отправляет Put запрос
        /// </summary>
        /// <param name="uri">URI для запроса</param>
        /// <param name="json">Тело запроса</param>
        /// <returns></returns>
        private static async Task<string> PostAnswer(string uri, string Method, string json)
        {
            WebRequest request = WebRequest.Create(uri);
            request.Method = Method;
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            var str = await GetAnswer(request);
            return str;
        }
    }

    public class ReferencesAPIResponse<T>
    {
        [JsonProperty("IsWell")]
        public bool IsWell { get; set; }
        [JsonProperty("Body")]
        public T Body { get; set; }
        [JsonProperty("Error")]
        public ReferenceAPIError Error { get; set; }
    }

    public class ReferenceAPIError
    {
        [JsonProperty("Code")]
        public int Code { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }
    }
}
