﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkBot.Tools;

namespace VkBot
{
    class AppConfig
    {
        public static string TOKEN => "291717baa8d671684d738444f70807a616e91081fab2511ae23d8f6ab62e3bf6b45d30cbba2232be435e6";
        public static ulong GROUPID => 100346873;
        private static Settings settings;
        public static Settings Settings
        {
            get
            {
                if (settings == null)
                    settings = SettingsHelper.Load();
                return settings;
            }
        }
    }
}
